<?php
/**
 * @file
 * expences.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function expences_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function expences_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function expences_node_info() {
  $items = array(
    'expence_entry' => array(
      'name' => t('Expence entry'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Description'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
