<?php

/**
 * @file
 * Helper query functions
 */

/**
 * Returns the total expences of the month of the year.
 *
 * @param int $month
 *   Specified month.
 * @param int $year
 *   Specified year.
 *
 * @return float|int
 *   Total expence.
 */
function expences_month_total($month = NULL, $year = NULL) {
  if (!isset($year)) {
    $year = date("Y");
  }
  if (isset($month)) {
    $from_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
    $to_date = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
  }
  else {
    $from_date = date("Y-m-1");
    $to_date = date("Y-m-d");
  }
  $query = _expences_prepare_query_between_dates($from_date, $to_date);
  $result = $query->execute();
  if ($result) {
    $nids = array_keys($result['node']);
    $sum = _expence_get_sum($nids);
  }
  else {
    $sum = 0;
  }
  return $sum;
}

/**
 * Returns the total expences of current year.
 *
 * @return float|int
 *   Total expences.
 */
function expences_current_year_total() {
  $from_date = date("Y-1-1");
  $to_date = date("Y-m-d");

  $query = _expences_prepare_query_between_dates($from_date, $to_date);
  $result = $query->execute();
  if ($result) {
    $nids = array_keys($result['node']);
    $sum = _expence_get_sum($nids);
  }
  else {
    $sum = 0;
  }
  return $sum;
}

/**
 * Retrieve currency symbol.
 *
 * @return string $currency
 */
function expences_get_currency_symbol() {
  $field_name = 'field_amount';
  $currency = field_info_instance('node', $field_name, 'expence_entry');
  return $currency['default_value'][0]['currency'];
}

/**
 * Returns average monthly total for the current year.
 *
 * @return int
 *   Average monthly total.
 */
function expences_get_monthly_average() {
  $current_month = date("m");
  $previous_mon = $current_month - 1;
  $sum = 0;
  $average = 0;
  for ($month = 1; $month <= $previous_mon; $month++) {
    $sum += expences_month_total($month);
  }
  if ($sum > 0) {
    $average = $sum / $previous_mon;
  }
  return $average;
}

/*
 * function expences_top_categories()
 * @param $flag
 * @param $catagory_range
 * @return $results
 */
function expences_top_categories($flag, $catagory_range) {
  //get all categories
  $vocab = taxonomy_vocabulary_machine_name_load('expence_category');
  $catagories = taxonomy_get_tree($vocab->vid);
  //initialising arrays
  $nids = array();
  $nids_array = array();
  $cat_name = array();

  $year = date("Y");
  $current_month = date("m");
  $previous_mon = $current_month - 1;

  for ($month = 1; $month <= $previous_mon; $month++) {
    $from_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
    $to_date = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));

    foreach ($catagories as $cat) {
      $result = expence_get_top_expence_nids($from_date, $to_date, $cat);
      if ($result) {
        $nids[] = array_keys($result['node']);
      }
    }
  }

  foreach ($nids as $key => $value) {
    foreach ($value as $nids_val) {
      $nids_array[] = $nids_val;
    }
  }
  if ($flag == 0) {
    $get_amount = expence_get_top_expence($nids_array, $catagory_range);
  }
  elseif ($flag == 1) {
    $get_amount = expence_get_least_expence($nids_array, $catagory_range);
  }
  $currency = expences_get_currency_symbol() . ' ';
  foreach ($get_amount as $key => $value) {
    $expence_amount = $value['field_amount_amount'];
    $expence_nid = $value['entity_id'];
    $get_tid = expence_get_tid($expence_nid);
    $get_name = expence_get_tid_name($get_tid);
    $output = $get_name . ': ' . $currency . $expence_amount;
    // Data ($data) here should be a array.
    $data[] = array(
      'data' => $output,
      'id' => drupal_html_id('expence'),
      'class' => array(drupal_html_class('catagories'), 'message-item'),
    );
  }
  $results = '';
  $results .= '<div class="yearly-expence" id="expence-catagory">';
  $results .= theme('item_list', array(
      'items' => $data,
      'type' => 'ul',
    )
  );
  $results .= '</div>';
  return $results;
}

/*
 * function expence_get_top_expence_nids()
 * @param $from_date
 * @param $to_date
 * @param $cat
 * @return $result
 */
function expence_get_top_expence_nids($from_date, $to_date, $cat) {
  $query = _expences_prepare_query_between_dates($from_date, $to_date);
  $query->fieldCondition('field_expence_catagory', 'tid', $cat->tid, '=');
  $result = $query->execute();
  return $result;
}

/**
 * Get top expence for the spcecified top category.
 *
 * @param $nids_array
 * @param $category_range
 *
 * @return $output
 */
function expence_get_top_expence($nids_array, $category_range) {
  $flag = 0;
  $query = db_select('node', 'n');
  $query->join('field_data_field_amount', 't', 't.entity_id=n.nid');
  $result = $query
    ->fields('n', array('nid'))
    ->fields('t', array('bundle', 'entity_id', 'field_amount_amount'))
    ->condition('entity_id', $nids_array, 'IN')
    ->condition('bundle', 'expence_entry')
    ->orderBy('field_amount_amount', 'DESC')
    ->range(0, $category_range)
    ->execute();
  while ($record = $result->fetchAssoc()) {
    $output[] = $record;
  }
  return $output;
}

/**
 * Retrieve term id.
 *
 * @param object $expence_nid
 *
 * @return $result
 */
function expence_get_tid($expence_nid) {
  // fetch category name of top expence category
  $query = db_select('field_data_field_expence_catagory', 'c');
  $result = $query
    ->fields('c', array('entity_id', 'field_expence_catagory_tid'))
    ->condition('entity_id', $expence_nid, '=')
    ->execute()->fetchAssoc();
  return $result;
}

/**
 * Retrieve term name.
 *
 * @param int $expence_tid
 *   Term id
 *
 * @return string $category->name
 */
function expence_get_tid_name($expence_tid) {
  $category = taxonomy_term_load($expence_tid['field_expence_catagory_tid']);
  return $category->name;
}

/**
 * Function to return least expence.
 *
 * @param array $nids_array
 * @param  array $category_range
 *
 * @return array $output
 */
function expence_get_least_expence($nids_array, $category_range) {
  $query = db_select('node', 'n');
  $query->join('field_data_field_amount', 't', 't.entity_id=n.nid');
  $result = $query
    ->fields('n', array('nid'))
    ->fields('t', array('bundle', 'entity_id', 'field_amount_amount'))
    ->condition('entity_id', $nids_array, 'IN')
    ->condition('bundle', 'expence_entry')
    ->orderBy('field_amount_amount', 'ASC')
    ->range(0, $category_range)
    ->execute();
  while ($record = $result->fetchAssoc()) {
    $output[] = $record;
  }
  return $output;
}

/**
 * Returns the sum of amount of all child terms of term provided.
 *
 * @param object $term
 *   Term object.
 * @param string $from_date
 *   From date.
 * @param string $to_date
 *   To date.
 *
 * @return float|int
 *   Total expence.
 */
function _get_child_terms_total($term, $from_date, $to_date) {
  $childs = taxonomy_get_children($term->tid);

  $tids = array();
  $tids[] = $term->tid;
  foreach ($childs as $child_term) {
    $tids[] = $child_term->tid;
  }

  $query = _expences_prepare_query_between_dates($from_date, $to_date);
  $query->fieldCondition('field_expence_catagory', 'tid', $tids, 'IN');
  $result = $query->execute();

  if (empty($result)) {
    return 0;
  }
  else {
    $nids = array_keys($result['node']);
    return _expence_get_sum($nids);
  }
}
