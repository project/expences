/**
 * @file
 * Scripting for generating charts.
 */
(function ($) {
  Drupal.behaviors.expencesPieCharts = {};
  Drupal.behaviors.expencesPieCharts.attach = function (context, settings) {
    var chart_setting = Drupal.settings.expences_chart_pie;
    $.jqplot.config.enablePlugins = true;
    $.each(chart_setting, function (index) {
      var element = chart_setting[index];
      var items = element.items;
      var keys = Object.keys(items);
      if (keys.length == 0) {
        return;
      }
      var data = new Array(keys.length);
      for (var i = 0; i < keys.length; i++) {
        data[i] = [keys[i], parseInt(items[keys[i]])];
      }
      if (data) {
        $.jqplot(element.container, [data], {
          seriesDefaults: {
            renderer: $.jqplot.PieRenderer,
            rendererOptions: {
              // Put data labels on the pie slices.
              // By default, labels show the percentage of the slice.
              showDataLabels: true
            },
            showMarker: true,
            pointLabels: {
              show: true
            }
          },
          legend: {
            show: true,
            placement: 'outside',
            rendererOptions: {
              numberRows: 1
            },
            location: 's',
            marginTop: '15px'
          }
        });
      }
    });
  };
})(jQuery);
