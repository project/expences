/**
 * @file
 * Scripting for generating charts.
 */
(function($) {
  Drupal.behaviors.expencesCharts = {};
  Drupal.behaviors.expencesCharts.attach = function(context, settings) {
    var chart_setting = Drupal.settings.expences_chart;
    var items = chart_setting.items;
    if (items) {
      var data = new Array();
      var ticks = new Array();
      for (var i = 0; i < items.length; i++) {
        ticks[i] = items[i].label;
        data[i] = parseInt(items[i].total);
      }
      jQuery.jqplot.config.enablePlugins = true;

      $.jqplot('chart-container', [data], {
        seriesDefaults: {
          renderer: $.jqplot.BarRenderer,
          pointLabels: {show: true},
          rendererOptions: {
            // Set varyBarColor to tru to use the custom colors on the bars.
            varyBarColor: true
          }
        },
        axes: {
          xaxis: {
            label: chart_setting.x_axis_label,
            renderer: $.jqplot.CategoryAxisRenderer,
            ticks: ticks
          }
        },
        highlighter: {show: false}
      });
    } else {
      alert('err');
    }
  };
})(jQuery);