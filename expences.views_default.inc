<?php
/**
 * @file
 * expences.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function expences_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'expence_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Expence report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Expence report';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'views_calc';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_date' => 'field_date',
    'title' => 'title',
    'field_amount' => 'field_amount',
    'field_expence_catagory' => 'field_expence_catagory',
    'field_payment_mode' => 'field_payment_mode',
    'field_brand' => 'field_brand',
    'field_shop_name' => 'field_shop_name',
  );
  $handler->display->display_options['style_options']['default'] = 'field_date';
  $handler->display->display_options['style_options']['info'] = array(
    'field_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 0,
      'calc' => array(),
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 1,
      'calc' => array(
        'COUNT' => 'COUNT',
      ),
    ),
    'field_amount' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 1,
      'calc' => array(
        'SUM' => 'SUM',
      ),
    ),
    'field_expence_catagory' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 0,
      'calc' => array(),
    ),
    'field_payment_mode' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 0,
      'calc' => array(),
    ),
    'field_brand' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 0,
      'calc' => array(),
    ),
    'field_shop_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
      'has_calc' => 0,
      'calc' => array(),
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['style_options']['detailed_values'] = '0';
  $handler->display->display_options['style_options']['precision'] = '0';
  /* Footer: Global: Result summary */
  $handler->display->display_options['footer']['result']['id'] = 'result';
  $handler->display->display_options['footer']['result']['table'] = 'views';
  $handler->display->display_options['footer']['result']['field'] = 'result';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_date']['id'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
  $handler->display->display_options['fields']['field_date']['field'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['settings'] = array(
    'format_type' => 'expenses_date',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Details';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Amount */
  $handler->display->display_options['fields']['field_amount']['id'] = 'field_amount';
  $handler->display->display_options['fields']['field_amount']['table'] = 'field_data_field_amount';
  $handler->display->display_options['fields']['field_amount']['field'] = 'field_amount';
  $handler->display->display_options['fields']['field_amount']['click_sort_column'] = 'amount';
  /* Field: Content: Expence Category */
  $handler->display->display_options['fields']['field_expence_catagory']['id'] = 'field_expence_catagory';
  $handler->display->display_options['fields']['field_expence_catagory']['table'] = 'field_data_field_expence_catagory';
  $handler->display->display_options['fields']['field_expence_catagory']['field'] = 'field_expence_catagory';
  $handler->display->display_options['fields']['field_expence_catagory']['delta_offset'] = '0';
  /* Field: Content: Payment mode */
  $handler->display->display_options['fields']['field_payment_mode']['id'] = 'field_payment_mode';
  $handler->display->display_options['fields']['field_payment_mode']['table'] = 'field_data_field_payment_mode';
  $handler->display->display_options['fields']['field_payment_mode']['field'] = 'field_payment_mode';
  /* Field: Content: Brand */
  $handler->display->display_options['fields']['field_brand']['id'] = 'field_brand';
  $handler->display->display_options['fields']['field_brand']['table'] = 'field_data_field_brand';
  $handler->display->display_options['fields']['field_brand']['field'] = 'field_brand';
  /* Field: Content: Shop Name */
  $handler->display->display_options['fields']['field_shop_name']['id'] = 'field_shop_name';
  $handler->display->display_options['fields']['field_shop_name']['table'] = 'field_data_field_shop_name';
  $handler->display->display_options['fields']['field_shop_name']['field'] = 'field_shop_name';
  /* Sort criterion: Content: Date (field_date) */
  $handler->display->display_options['sorts']['field_date_value']['id'] = 'field_date_value';
  $handler->display->display_options['sorts']['field_date_value']['table'] = 'field_data_field_date';
  $handler->display->display_options['sorts']['field_date_value']['field'] = 'field_date_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Sort criterion: Content: Updated date */
  $handler->display->display_options['sorts']['changed']['id'] = 'changed';
  $handler->display->display_options['sorts']['changed']['table'] = 'node';
  $handler->display->display_options['sorts']['changed']['field'] = 'changed';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'expence_entry' => 'expence_entry',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Date (field_date) */
  $handler->display->display_options['filters']['field_date_value']['id'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['table'] = 'field_data_field_date';
  $handler->display->display_options['filters']['field_date_value']['field'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['operator'] = 'between';
  $handler->display->display_options['filters']['field_date_value']['group'] = 1;
  $handler->display->display_options['filters']['field_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_date_value']['expose']['operator_id'] = 'field_date_value_op';
  $handler->display->display_options['filters']['field_date_value']['expose']['label'] = 'Filter date';
  $handler->display->display_options['filters']['field_date_value']['expose']['operator'] = 'field_date_value_op';
  $handler->display->display_options['filters']['field_date_value']['expose']['identifier'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_date_value']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_date_value']['default_date'] = 'now -7';
  $handler->display->display_options['filters']['field_date_value']['default_to_date'] = 'now';
  /* Filter criterion: Content: Has taxonomy terms (with depth) */
  $handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['group'] = 1;
  $handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator_id'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Expence Category';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
  $handler->display->display_options['filters']['term_node_tid_depth']['vocabulary'] = 'expence_category';
  $handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
  $handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '3';
  /* Filter criterion: Content: Payment mode (field_payment_mode) */
  $handler->display->display_options['filters']['field_payment_mode_tid']['id'] = 'field_payment_mode_tid';
  $handler->display->display_options['filters']['field_payment_mode_tid']['table'] = 'field_data_field_payment_mode';
  $handler->display->display_options['filters']['field_payment_mode_tid']['field'] = 'field_payment_mode_tid';
  $handler->display->display_options['filters']['field_payment_mode_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_payment_mode_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_payment_mode_tid']['expose']['operator_id'] = 'field_payment_mode_tid_op';
  $handler->display->display_options['filters']['field_payment_mode_tid']['expose']['label'] = 'Payment mode';
  $handler->display->display_options['filters']['field_payment_mode_tid']['expose']['operator'] = 'field_payment_mode_tid_op';
  $handler->display->display_options['filters']['field_payment_mode_tid']['expose']['identifier'] = 'field_payment_mode_tid';
  $handler->display->display_options['filters']['field_payment_mode_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_payment_mode_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_payment_mode_tid']['vocabulary'] = 'payment_method';
  /* Filter criterion: Content: Shop Name (field_shop_name) */
  $handler->display->display_options['filters']['field_shop_name_tid']['id'] = 'field_shop_name_tid';
  $handler->display->display_options['filters']['field_shop_name_tid']['table'] = 'field_data_field_shop_name';
  $handler->display->display_options['filters']['field_shop_name_tid']['field'] = 'field_shop_name_tid';
  $handler->display->display_options['filters']['field_shop_name_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_shop_name_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_shop_name_tid']['expose']['operator_id'] = 'field_shop_name_tid_op';
  $handler->display->display_options['filters']['field_shop_name_tid']['expose']['label'] = 'Shop Name';
  $handler->display->display_options['filters']['field_shop_name_tid']['expose']['operator'] = 'field_shop_name_tid_op';
  $handler->display->display_options['filters']['field_shop_name_tid']['expose']['identifier'] = 'field_shop_name_tid';
  $handler->display->display_options['filters']['field_shop_name_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_shop_name_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_shop_name_tid']['vocabulary'] = 'shop';
  $handler->display->display_options['filters']['field_shop_name_tid']['hierarchy'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'expence-report';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Expence report';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['expence_report'] = $view;

  return $export;
}
