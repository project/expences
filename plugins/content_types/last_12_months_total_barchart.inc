<?php

/**
 * @file
 * Last 12 months total bar chart.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Last 12 months total bar chart'),
  'description' => t('Display bar chart of totals of past 12 months.'),
  'render callback' => 'last_12_months_total_barchart_content_type_render',
  'category' => array(t('Expences'), -9),
);

/**
 * Implements hook_content_type_render().
 */
function last_12_months_total_barchart_content_type_render($subtype, $conf, $args, $context) {
  //Including the required file.
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $block = new stdClass();
  $block->title = t('Last 12 months totals bar chart');
  $block->content = '<div id="chart-container"></div>';

  // Get last 12 months total
  $items = array();
  $series = '';
  $labels = '';

  for ($i = 1; $i <= 12; $i++) {
    $date = date("M Y", strtotime("-$i months"));
    $month = date("m", strtotime($date));
    $year = date("Y", strtotime($date));
    $sum = expences_month_total($month, $year);

    $items[] = array(
      'total' => $sum,
      'label' => $date,
    );
    $series .= $sum . ',';
    $labels .= $date . ',';
  }

  drupal_add_library('jqplot', 'jqplot.barRenderer');
  drupal_add_library('jqplot', 'jqplot.categoryAxisRenderer');
  drupal_add_library('jqplot', 'jqplot.canvasAxisTickRenderer');
  drupal_add_js(array(
    'expences_chart' => array(
      'items' => $items,
      'series' => $series,
      'labels' => $labels,
      'x_axis_label' => 'Months',
    )
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'expences') . '/js/expences_charts.js', 'file');
  drupal_add_css(drupal_get_path('module', 'expences') . '/css/expences.css', 'file');

  return $block;
}
