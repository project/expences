<?php

/**
 * @file
 * Current month total expence pane.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Current Month Total'),
  'description' => t('Display the monthly total expences.'),
  'render callback' => 'current_month_total_content_type_render',
  'category' => array(t('Expences'), -9),
);

/**
 * Implements hook_content_type_render().
 */
function current_month_total_content_type_render($subtype, $conf, $args, $context) {
  //Including the required file.
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $block = new stdClass();
  $block->title = t('Total expences of the month');
  $block->content = expences_get_currency_symbol() . ' ';
  $block->content .= number_format(expences_month_total(), 0);
  return $block;
}
