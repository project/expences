<?php

/**
 * @file
 * expences dahsboard
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t(' Average Monthly Total of the year'),
  'description' => t('Display the Average Monthly Total of the year.'),
  'render callback' => 'average_month_total_content_type_render',
  'category' => array(t('Expences'), -9),
);


/**
 * Implements hook_content_type_render().
 * @return $block;
 */
function average_month_total_content_type_render($subtype, $conf, $args, $context) {
  //Including the required file.
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $block = new stdClass();
  $block->title = t('Average monthly total');
  $block->content = expences_get_currency_symbol() . ' ';
  $block->content .= number_format(expences_get_monthly_average(), 0);
  return $block;
}
