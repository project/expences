<?php

/**
 * @file
 * plugin To show least expences  of current year.
 * The number of least expences are configurable into plugin settings.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Yearly Least expences categories'),
  'description' => t('Display the Least expences category.'),
  'render callback' => 'least_expence_categories_content_type_render',
  'edit form' => 'least_expence_categories_form_edit_form',
  'category' => array(t('Expences'), -9),
);

/**
 * Implements hook_content_type_render().
 * @param $conf
 * @param $context
 * @param $args
 * @return $block;
 */
function least_expence_categories_content_type_render($subtype, $conf, $args, $context) {
  //Including the required file.
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $catagory_range = $conf['least_expence_category'];
  $block = new stdClass();
  $block->title = t('Yearly Least Category Expences');
  $block->content = expences_top_categories($flag = 1, $catagory_range);
  return $block;
}


function least_expence_categories_form_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['least_expence_category'] = array(
    '#title' => t('Set number of least expence to be show'),
    '#type' => 'textfield',
    '#description' => t('Enter the number of least expence category eg: 6'),
    '#default_value' => !empty($conf['least_expence_category']) ? $conf['least_expence_category'] : 3,
    '#size' => 10,
    '#element_validate' => array('least_expence_categories_form_element_validate_integer'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function least_expence_categories_form_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['least_expence_category'] = $form_state['values']['least_expence_category'];
}

/*
* function least_expence_categories_form_element_validate_integer() handles integer element validations.
* @param array $element
* @param array $form_state
*/
function least_expence_categories_form_element_validate_integer($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value <= 0)) {
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
  if($value > 10) {
    form_error($element, t('Category should be less then 10', array('%name' => $element['#title'])));
  }
}
