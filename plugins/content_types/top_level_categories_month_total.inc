<?php

/**
 * @file
 * Current year total expence pane.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Top Level Categories for month total'),
  'description' => t('Display total expences of top level categories.'),
  'render callback' => 'top_level_categories_month_total_content_type_render',
  'edit form' => 'top_level_categories_month_total_content_type_edit',
  'category' => array(t('Expences'), -9),
);

/**
 * Implements hook_content_type_render().
 */
function top_level_categories_month_total_content_type_render($subtype, $conf, $args, $context) {
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $block = new stdClass();
  $block->title = t('Top level categories total of the month');

  $vocab = taxonomy_vocabulary_machine_name_load('expence_category');
  $top_level_terms = taxonomy_get_tree($vocab->vid, 0, 1);

  $totals = array();
  $from_date = date("Y-m-1");
  $to_date = date("Y-m-d");
  foreach ($top_level_terms as $term) {
    $sum = _get_child_terms_total($term, $from_date, $to_date);
    if ($sum) {
      $totals [$term->name] = $sum;
    }
  }
  arsort($totals);
  $items = array();
  foreach ($totals as $key => $sum) {
    $items [] = $key . ' : ' . number_format($sum, 0);
  }
  $output = theme('item_list', array('items' => $items));
  $block->content = $output;

  if ($conf['show_chart']) {
    $id = drupal_html_id('expences-pie-chart');
    $chart = '<div id="' . $id . '"></div>';
    drupal_add_library('jqplot', 'jqplot.pieRenderer');
    drupal_add_js(array(
      'expences_chart_pie' => array(
        'monthly-chart' => array(
          'items' => $totals,
          'container' => $id,
        )
      )
    ), 'setting');
    drupal_add_js(drupal_get_path('module', 'expences') . '/js/expences_pie_charts.js', 'file');
    drupal_add_css(drupal_get_path('module', 'expences') . '/css/expences.css', 'file');

    $block->content .= $chart;
  }
  return $block;
}

function top_level_categories_month_total_content_type_edit($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['show_chart'] = array(
    '#title' => t('Show pie chart?'),
    '#type' => 'checkbox',
    '#description' => t('Do you want to show pie chart also?'),
    '#default_value' => !empty($conf['show_chart']) ? $conf['show_chart'] : 1,
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function top_level_categories_month_total_content_type_edit_submit(&$form, &$form_state) {
  $form_state['conf']['show_chart'] = $form_state['values']['show_chart'];
}
