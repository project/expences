<?php

/**
 * @file
 * Current year total expence pane.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */

$plugin = array(
  'title' => t('Current Year Total'),
  'description' => t('Display total expences of the current year.'),
  'render callback' => 'current_year_total_content_type_render',
  'category' => array(t('Expences'), -9),
);

/**
 * Implements hook_content_type_render().
 * @return $block;
 */
function current_year_total_content_type_render($subtype, $conf, $args, $context) {
  module_load_include('inc', 'expences', '/includes/expences_helper_functions');
  $block = new stdClass();
  $block->title = t('Total expences of the year');
  $block->content = expences_get_currency_symbol() . ' ';
  $block->content .= number_format(expences_current_year_total(), 0);
  return $block;
}
