This module provides facility to maintain financial expences records in drupal.
It provides new content type "Expence Entry" for entering details about an expence.
It also provides bar charts for monthly report and for expenses between custom dates.

Dependencies
---------------
Money https://drupal.org/project/money
Date
jqplot

Installation
----------------
First install and configure date, money and jqplot modules.
Download and place jqplot charting library in libraries folder.

Configuration
----------------
Goto 'admin/structure/taxonomy/payment_method' and add terms for payment methods like Cash, Bank1 debit card, Bank2 Internet banking, etc...


TODO : Some months back
1. Readme.txt README.txt : Done
2. Add more types of chart
3. Dashboard


TODO update 18th Oct. 2014
1. expences chart :- top level category checkbox
2. dashboard :- panel page, ctool pane : done
3. Current Month total : done
4. Average mothly total yearly excluding current month : done
5. Top expence category (mothly & yearly) : inprogress
6. least expence category (mothly & yearly) :inprogress
