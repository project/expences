<?php
/**
 * @file
 * expences.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function expences_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'expences_dashboard';
  $page->task = 'page';
  $page->admin_title = 'Expences Dashboard';
  $page->admin_description = 'Dashboard of all expences.';
  $page->path = 'expences/dashboard';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Expences Dashboard',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_expences_dashboard_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'expences_dashboard';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 2,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'top' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '4e8ed844-7670-438a-9a17-9605fe2792b6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-27b86f03-13ea-4f2e-9585-090664330be7';
    $pane->panel = 'bottom';
    $pane->type = 'last_12_months_total_barchart';
    $pane->subtype = 'last_12_months_total_barchart';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '27b86f03-13ea-4f2e-9585-090664330be7';
    $display->content['new-27b86f03-13ea-4f2e-9585-090664330be7'] = $pane;
    $display->panels['bottom'][0] = 'new-27b86f03-13ea-4f2e-9585-090664330be7';
    $pane = new stdClass();
    $pane->pid = 'new-c657b159-a23c-4a8f-8c2d-b34be63f8829';
    $pane->panel = 'left';
    $pane->type = 'current_month_total';
    $pane->subtype = 'current_month_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c657b159-a23c-4a8f-8c2d-b34be63f8829';
    $display->content['new-c657b159-a23c-4a8f-8c2d-b34be63f8829'] = $pane;
    $display->panels['left'][0] = 'new-c657b159-a23c-4a8f-8c2d-b34be63f8829';
    $pane = new stdClass();
    $pane->pid = 'new-c703e0ed-6388-45e9-ad31-c5654620a464';
    $pane->panel = 'left';
    $pane->type = 'average_monthly_total';
    $pane->subtype = 'average_monthly_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c703e0ed-6388-45e9-ad31-c5654620a464';
    $display->content['new-c703e0ed-6388-45e9-ad31-c5654620a464'] = $pane;
    $display->panels['left'][1] = 'new-c703e0ed-6388-45e9-ad31-c5654620a464';
    $pane = new stdClass();
    $pane->pid = 'new-c1288b4f-6c93-40ff-8251-1d10bc4a04f3';
    $pane->panel = 'left';
    $pane->type = 'current_year_total';
    $pane->subtype = 'current_year_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c1288b4f-6c93-40ff-8251-1d10bc4a04f3';
    $display->content['new-c1288b4f-6c93-40ff-8251-1d10bc4a04f3'] = $pane;
    $display->panels['left'][2] = 'new-c1288b4f-6c93-40ff-8251-1d10bc4a04f3';
    $pane = new stdClass();
    $pane->pid = 'new-45120154-08db-4813-a86e-cd23577559e3';
    $pane->panel = 'left';
    $pane->type = 'top_level_categories_month_total';
    $pane->subtype = 'top_level_categories_month_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'show_chart' => 1,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '45120154-08db-4813-a86e-cd23577559e3';
    $display->content['new-45120154-08db-4813-a86e-cd23577559e3'] = $pane;
    $display->panels['left'][3] = 'new-45120154-08db-4813-a86e-cd23577559e3';
    $pane = new stdClass();
    $pane->pid = 'new-0eb9b9ba-e10a-4e73-847f-3f17e2e3f05f';
    $pane->panel = 'right';
    $pane->type = 'top_level_categories_year_total';
    $pane->subtype = 'top_level_categories_year_total';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'show_chart' => 1,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0eb9b9ba-e10a-4e73-847f-3f17e2e3f05f';
    $display->content['new-0eb9b9ba-e10a-4e73-847f-3f17e2e3f05f'] = $pane;
    $display->panels['right'][0] = 'new-0eb9b9ba-e10a-4e73-847f-3f17e2e3f05f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['expences_dashboard'] = $page;

  return $pages;

}
